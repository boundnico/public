$(function () {
// ------

var dweet_thing = 'sbjohnny';
var $result = $('.result');

function reload() {
  $result.empty();
  $result.text('Please wait...');
  $result.addClass('wait');
  
  dweetio.get_latest_dweet_for(dweet_thing, function(err, dweet){
    $result.removeClass('wait');  
    $result.empty();
    
    if (err) {
      $error = $('<span class="error">').text('ERROR: ' + err);
      $error.appendTo($result);
      return;
    }
    
    var dweet = dweet[0]; // Dweet is always an array of 1
   
    var $link = $('<a>');
    $link
      .attr('href', dweet.content.link)
      .text(dweet.content.link);
    $link.appendTo($result);
  });
}

function copyTextToClipboard(text) {
  var textArea = document.createElement("textarea");

  //
  // *** This styling is an extra step which is likely not required. ***
  //
  // Why is it here? To ensure:
  // 1. the element is able to have focus and selection.
  // 2. if element was to flash render it has minimal visual impact.
  // 3. less flakyness with selection and copying which **might** occur if
  //    the textarea element is not visible.
  //
  // The likelihood is the element won't even render, not even a flash,
  // so some of these are just precautions. However in IE the element
  // is visible whilst the popup box asking the user for permission for
  // the web page to copy to the clipboard.
  //

  // Place in top-left corner of screen regardless of scroll position.
  textArea.style.position = 'fixed';
  textArea.style.top = 0;
  textArea.style.left = 0;

  // Ensure it has a small width and height. Setting to 1px / 1em
  // doesn't work as this gives a negative w/h on some browsers.
  textArea.style.width = '2em';
  textArea.style.height = '2em';

  // We don't need padding, reducing the size if it does flash render.
  textArea.style.padding = 0;

  // Clean up any borders.
  textArea.style.border = 'none';
  textArea.style.outline = 'none';
  textArea.style.boxShadow = 'none';

  // Avoid flash of white box if rendered for any reason.
  textArea.style.background = 'transparent';


  textArea.value = text;

  document.body.appendChild(textArea);
  textArea.focus();
  textArea.select();

  try {
    var successful = document.execCommand('copy');
    var msg = successful ? 'successful' : 'unsuccessful';
    console.log('Copying text command was ' + msg);
  } catch (err) {
    console.log('Oops, unable to copy');
  }

  document.body.removeChild(textArea);
}

reload();

dweetio.listen_for(dweet_thing, function(dweet){
  $result.removeClass('wait');  
  $result.empty();

  var $link = $('<a>');
  $link
    .attr('href', dweet.content.link)
    .text(dweet.content.link);
  $link.appendTo($result);
});

$('#reload-button').on('click', function() {
  reload();
});

$('#copy-button').on('click', function() {
  copyTextToClipboard($result.text());
});

$('#update-button').on('click', function() {
  var link = $('#update-field').val();
  $('#update-field').val('');
  
  if (!link) {
    return;
  }
  
  var data = {
    link: link
  };
  dweetio.dweet_for(dweet_thing, data, function(err, dweet){
    console.log(dweet.thing); // "my-thing"
    console.log(dweet.content); // The content of the dweet
    console.log(dweet.created); // The create date of the dweet
  });
});

$('#update-field').on('keypress', function(event) {
  if (event.which === 13) {
    $('#update-button').click();
  }
});

// ------
});
